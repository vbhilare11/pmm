


resource_group_location = "eastus"

resource_group_name = "staging-eastus"

virtual_network = "staging-vnet"

azurerm_network_interface = "staging-vnet-int"

azurerm_linux_virtual_machine = "staging-pmm-server"

os_profile  = "pmm-server"

network_security_group = "staging-nsg"

username = "azureuser"

