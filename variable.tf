# Create your variables 

variable "resource_group_location" {
    type = string
    default = "eastus"
    description = "Location of the resource group"
}

variable "resource_group_name" {
    type = string
}

variable "virtual_network" {
    type = string
    default = "vbhilare-vnet"
    description = "Virtual Network"
}

variable "azurerm_network_interface" {
    type = string
    default = ""  
}

variable "azurerm_linux_virtual_machine" {
    type = string
    default = "pmm-server"
    description = "Name of the VM"
}

variable "os_profile" {
    type = string
    default = "pmm_server"
    description = "Hostname for the VM"
}

variable "network_security_group" {
    type = string
    default = "" 
}

variable "username" {
    type = string
    default = "azureuser"
    description = "username for the VM"
}

